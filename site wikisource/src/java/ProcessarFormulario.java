import org.apache.commons.mail.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ProcessarFormulario", urlPatterns = {"/paginas/processarFormulario.do"})
public class ProcessarFormulario extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isInfval = false;
        boolean isReceber = false;

        String nome = request.getParameter("nome");
        String emailx = request.getParameter("email");
        String assunto = request.getParameter("assunto");
        String mensagem = request.getParameter("mensagem");
        String infval = request.getParameter("infval");
        isInfval = infval.equals("S");
        String receber = request.getParameter("receber");
        isReceber = receber != null;
        
        
        
        System.out.println("Nome: " + nome);
        System.out.println("Email: " + emailx);
        System.out.println("Assunto: " + assunto);
        System.out.println("Mensagem: " + mensagem);
        System.out.println("Informações Validas: " + infval);
        System.out.println("Receber: " + receber);

        String myEmailId = "marceloferreira.uc14@gmail.com";
        String myPassword = "pms159753";
        String senderId = "issuesmaker@gmail.com";
        String senderIdSL =emailx;
        /*
        issuesmaker@gmail.com
        */
                
        try {
            // Create the email message
            HtmlEmail email = new HtmlEmail();
            email.setSmtpPort(587);
            email.setHostName("smtp.gmail.com");
            email.addTo(senderId);
            email.setFrom(myEmailId);
            email.setSubject("UC14 - Marcelo Ferreira");
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setTLS(true);

           
            email.setHtmlMsg("<tr>"
                            + "<b>Nome:</b>" + nome
                            +"</tr>"
                            +"<tr>"
                            + "<b>Email:</b>" + emailx
                            +"</tr>"
                            + "<tr>"
                            +"<b>Assunto: </b>" + assunto
                            + "</tr>"
                            + "<tr>"
                            + "<b>Mensagem: </b>" + mensagem 
                            + "</tr>"
                            + "<tr>"
                            + "<b>Informações Validas:</b>" + infval 
                            + "</tr>"
                            + "<tr>"
                            + "<b>Deseja Receber:</b>" + receber
                            + "</tr>");

            // set the alternative message
            email.setTextMsg("Seu Ciente de E-mail Não Suporta Mensagens HTML.");

            // send the email
            email.send();
            System.out.println("E-mail Enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }
        try {
            // Create the email message
            HtmlEmail email = new HtmlEmail();
            email.setSmtpPort(587);
            email.setHostName("smtp.gmail.com");
            email.addTo(senderIdSL);
            email.setFrom(myEmailId);
            email.setSubject("Recebemos Sua Solicitação - Favor Não Responder");
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setTLS(true);

           
            email.setHtmlMsg(
                    String.format("<html>\n"
                            + "    <head>\n"
                            + "        <title>Suporte</title>\n"
                            + "        <meta charset=\"UTF-8\">\n"
                            + "    </head>\n"
                            + "    <body  >\n"
                            + "\n"
                            + "        <h2>Olá " + nome+ " ,  </h2>\n"
                            + "        <hr />\n"
                            + "        <h4><p>A equipe comando recebeu seu e-mail e estamos trabalhando para atender sua solicitação.</p></h4>\n"
                            + "        <br/>\n"
                            + "        <br/>\n"
                            + "        <br/>\n"
                            + "\n"                          
                            + "    <br/>\n" 
                            + "    <b>\n"
                            + "        Atenciosamente, <br />\n"
                            + "        <br/>\n"
                            + "        Equipe Comandos.\n"
                            + "    </b>\n"
                            + "     <br/>\n"
                            + "    <br/>\n"
                            + "    <br/>\n"
                            + "    <hr />\n"
                            + "    <center>\n"
                            + "        <b>© COPYRIGHT 2019, COMANDOS DA UNIDADE CURRICULAR 14 S.A.</b>\n"
                            + "    </center>\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "</body>\n"
                            + "</html>", "dsds"));

            // set the alternative message
            email.setTextMsg("Seu Ciente de E-mail Nao Suporta Mensagens HTML.");

            // send the email
            email.send();
            System.out.println("E-mail Enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }


    }

    @Override
    public String getServletInfo() {
   
        return "Short description";
    }// </editor-fold>
     
}